// src/screens/LeaderboardScreen.tsx
import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, StyleSheet, Button} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNavigation} from '@react-navigation/native';

interface LeaderboardEntry {
    score: number;
    timeStamp: string;
}

export default function LeaderboardScreen() {
    const [leaderboard, setLeaderboard] = useState<LeaderboardEntry[]>([]);
    const navigation = useNavigation();

    useEffect(() => {
        const loadLeaderboard = async () => {
            try {
                const storedLeaderboard = await AsyncStorage.getItem('leaderboard');
                if (storedLeaderboard) {
                    setLeaderboard(JSON.parse(storedLeaderboard));
                }
            } catch (error) {
                console.error('Failed to load leaderboard:', error);
            }
        };

        loadLeaderboard();
    }, []);


    const renderItem = ({item, index}: { item: LeaderboardEntry, index: number }) => {
        const isLastItem = index === leaderboard.length - 1;
        return (
            <View style={[styles.itemContainer, isLastItem && styles.lastScore]}>
                <Text
                    style={styles.itemText}>{`Score: ${item.score} - Date: ${new Date(item.timeStamp).toLocaleString()}`}</Text>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Leaderboard</Text>
            <FlatList
                data={leaderboard}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderItem}
            />
            <Button
                title="Back to Quiz"
                onPress={() => navigation.navigate('Quiz')}
                style={styles.backButton}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 16,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
    },
    itemContainer: {
        padding: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
    },
    itemText: {
        fontSize: 18,
    },
    backButton: {
        marginTop: 20,
    },
    lastScore: {
        backgroundColor: "azure"
    }
});

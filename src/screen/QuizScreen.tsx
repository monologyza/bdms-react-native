import React, {useState, useEffect} from 'react';
import {View, Text, Button, FlatList, TouchableOpacity, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import {sampleQuestions} from "../constants";

const shuffleArray = (array: any[]) => array.sort(() => Math.random() - 0.5);

const getRandomQuestion = () => {
    return shuffleArray(sampleQuestions).slice(0, 20).map(question => ({
        ...question,
        answers: shuffleArray([...question.answers])
    }));
};

export default function QuizScreen() {
    const [questions, setQuestions] = useState<any[]>([]);
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
    const [selectedAnswer, setSelectedAnswer] = useState<string | null>(null);
    const [score, setScore] = useState<number>(0);
    const navigation = useNavigation();

    useEffect(() => {
        const newQuestions: any = getRandomQuestion();
        setQuestions(newQuestions);
        setCurrentQuestionIndex(0);
    }, []);

    const handleAnswerSelect = (answer: string) => {
        setSelectedAnswer(answer);
        if (answer === questions[currentQuestionIndex].correctAnswer) {
            setScore(prevScore => prevScore + 1);
        }
    }

    const handleNextQuestion = () => {
        if (currentQuestionIndex < questions.length - 1) {
            setCurrentQuestionIndex(currentQuestionIndex + 1);
            setSelectedAnswer(null);
        } else {
            handleFinishQuiz();
        }
    }

    const saveScore = async (currentScore: number) => {
        try {
            const storedLeaderboard = await AsyncStorage.getItem('leaderboard');
            const leaderboard = storedLeaderboard ? JSON.parse(storedLeaderboard) : [];
            leaderboard.push({score: currentScore, timeStamp: new Date().toISOString()});
            await AsyncStorage.setItem('leaderboard', JSON.stringify(leaderboard));
        } catch (error) {
            console.error("Failed to save score", error);
        }
    }

    const handleFinishQuiz = async () => {
        await saveScore(score);
        navigation.navigate('Leaderboard')
    }
    //style function for answer button
    const getAnswerButtonStyle = (answer: string) => {
        if (!selectedAnswer) {
            return styles.answerButton; // Default style
        }
        if (answer === questions[currentQuestionIndex].correctAnswer) {
            return [styles.answerButton, styles.correctAnswerButton];
        }
        if (selectedAnswer === answer) {
            return [styles.answerButton, styles.incorrectAnswerButton];
        }
        return styles.answerButton;
    };

    const resetQuiz = () => {
        const newQuestions = getRandomQuestion();
        setQuestions(newQuestions);
        setCurrentQuestionIndex(0);
        setSelectedAnswer(null);
        setScore(0);
    }

    if (questions.length === 0) {
        return <Text>Loading...</Text>;
    }

    const currentQuestion = questions[currentQuestionIndex];

    return (
        <View style={styles.container}>
            <Text style={styles.questionText}>{currentQuestion.question}</Text>
            {currentQuestion.answers.map((answer: string, idx: number) => (
                <>
                    <TouchableOpacity key={idx}
                                      onPress={() => !selectedAnswer && handleAnswerSelect(answer)}
                                      style={getAnswerButtonStyle(answer)}>
                        <Text style={styles.answerText}>{answer}</Text>
                    </TouchableOpacity>
                </>
            ))}
            <Text style={styles.questionCounter}>
                {`Question ${currentQuestionIndex + 1} of ${questions.length}`}
            </Text>
            {selectedAnswer && (
                <Button
                    title={currentQuestionIndex < questions.length - 1 ? "Next Question" : "Finish Quiz"}
                    onPress={handleNextQuestion}></Button>
            )}
            <Button title="Restart Quiz"
                    onPress={resetQuiz}
                    style={styles.restartButton}/>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 16,
        justifyContent: 'center',
    },
    questionText: {
        fontSize: 22,
        fontWeight: 'bold',
        marginBottom: 20,
    },
    answerButton: {
        padding: 10,
        backgroundColor: '#ddd',
        marginVertical: 6,
        borderRadius: 5,
    },
    correctAnswerButton: {
        backgroundColor: 'green',
    },
    incorrectAnswerButton: {
        backgroundColor: 'red',
    },
    answerText: {
        fontSize: 18,
    },
    questionCounter: {
        fontSize: 16,
        marginVertical: 10,
        textAlign: 'center',
    },
    restartButton: {
        marginTop: 20,
    },
});

export const sampleQuestions = [
    {
        question: 'What is the capital of Japan?',
        answers: ['Tokyo', 'Kyoto', 'Osaka', 'Nagoya'],
        correctAnswer: 'Tokyo',
    },
    {
        question: 'Which planet is known as the Red Planet?',
        answers: ['Mars', 'Venus', 'Jupiter', 'Saturn'],
        correctAnswer: 'Mars',
    },
    {
        question: 'What is the largest ocean on Earth?',
        answers: ['Atlantic Ocean', 'Indian Ocean', 'Arctic Ocean', 'Pacific Ocean'],
        correctAnswer: 'Pacific Ocean',
    },
    {
        question: 'Who wrote "Romeo and Juliet"?',
        answers: ['William Shakespeare', 'Charles Dickens', 'Jane Austen', 'Mark Twain'],
        correctAnswer: 'William Shakespeare',
    },
    {
        question: 'What is the chemical symbol for gold?',
        answers: ['Au', 'Ag', 'Pb', 'Fe'],
        correctAnswer: 'Au',
    },
    {
        question: 'Which country is known as the Land of the Rising Sun?',
        answers: ['China', 'Australia', 'Japan', 'Thailand'],
        correctAnswer: 'Japan',
    },
    {
        question: 'What is the largest continent?',
        answers: ['Africa', 'Asia', 'Europe', 'Antarctica'],
        correctAnswer: 'Asia',
    },
    {
        question: 'Who was the first President of the United States?',
        answers: ['Thomas Jefferson', 'Abraham Lincoln', 'George Washington', 'John Adams'],
        correctAnswer: 'George Washington',
    },
    {
        question: 'What is the speed of light?',
        answers: ['300,000 km/s', '150,000 km/s', '500,000 km/s', '100,000 km/s'],
        correctAnswer: '300,000 km/s',
    },
    {
        question: 'Which element has the atomic number 1?',
        answers: ['Oxygen', 'Carbon', 'Hydrogen', 'Nitrogen'],
        correctAnswer: 'Hydrogen',
    },
    {
        question: 'What is the smallest prime number?',
        answers: ['1', '2', '3', '5'],
        correctAnswer: '2',
    },
    {
        question: 'Which planet is closest to the Sun?',
        answers: ['Mercury', 'Venus', 'Earth', 'Mars'],
        correctAnswer: 'Mercury',
    },
    {
        question: 'In which year did World War II end?',
        answers: ['1942', '1944', '1945', '1946'],
        correctAnswer: '1945',
    },
    {
        question: 'What is the hardest natural substance on Earth?',
        answers: ['Gold', 'Iron', 'Diamond', 'Platinum'],
        correctAnswer: 'Diamond',
    },
    {
        question: 'Which gas is most abundant in the Earth’s atmosphere?',
        answers: ['Oxygen', 'Carbon Dioxide', 'Nitrogen', 'Hydrogen'],
        correctAnswer: 'Nitrogen',
    },
    {
        question: 'Who painted the Mona Lisa?',
        answers: ['Vincent van Gogh', 'Pablo Picasso', 'Leonardo da Vinci', 'Claude Monet'],
        correctAnswer: 'Leonardo da Vinci',
    },
    {
        question: 'What is the capital of Canada?',
        answers: ['Toronto', 'Vancouver', 'Ottawa', 'Montreal'],
        correctAnswer: 'Ottawa',
    },
    {
        question: 'Which country invented the tea?',
        answers: ['India', 'England', 'China', 'Japan'],
        correctAnswer: 'China',
    },
    {
        question: 'Who discovered penicillin?',
        answers: ['Marie Curie', 'Isaac Newton', 'Albert Einstein', 'Alexander Fleming'],
        correctAnswer: 'Alexander Fleming',
    },
    {
        question: 'What is the tallest mountain in the world?',
        answers: ['K2', 'Kangchenjunga', 'Mount Everest', 'Makalu'],
        correctAnswer: 'Mount Everest',
    },
];

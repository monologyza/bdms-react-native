import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from "@react-navigation/stack";
import QuizScreen from '../screen/QuizScreen';
import LeaderboardScreen from '../screen/LeaderboardScreen';

const Stack = createStackNavigator();

export default function AppNavigator() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Quiz">
                <Stack.Screen name="Quiz" component={QuizScreen} options={{ title: 'Quiz' }} />
                <Stack.Screen name="Leaderboard" component={LeaderboardScreen} options={{ title: 'Leaderboard' }} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}
